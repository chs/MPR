# encoding: utf-8

"""
File Name   : test_create_graphviz_plot.py
Project Name: MPR
Description : test suite for create_graphviz_plot.py script
Author      : Robert Schweppe (robert.schweppe@ufz.de)
Created     : 2019-03-30 13:30
"""

# IMPORTS
import pytest
from src_python.pre_proc.create_graphviz_plot import Redirecter, GraphvizPlotter, DataArray

class TestRedirecter:
    def test_one(self):
        assert True


class TestGraphvizPlotter:
    def test_one(self):
        assert True


class TestDataArray:
    def test_one(self):
        assert True
