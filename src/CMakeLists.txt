# generate transfer function fortran code with python on the fly
include(../cmake-mpr-modules/generateTFS.cmake)

set (LIB_NAME mpr)
file (GLOB sources ./mo_mpr_*.f90 ./mo_mpr_*.F90)

option(BUILD_MPR_LIB_SHARED "Build MPR library as shared." OFF)
if(BUILD_MPR_LIB_SHARED)
  add_library(${LIB_NAME} SHARED ${sources})
else()
  add_library(${LIB_NAME} STATIC ${sources})
endif()
target_include_directories(${LIB_NAME} PUBLIC ${CMAKE_CURRENT_BINARY_DIR})
# target_include_directories(${LIB_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

include(../cmake/CPM.cmake)

set (TARGET_NAME forces)
if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/../${TARGET_NAME}")
  message(STATUS "${LIB_NAME}: found local ${TARGET_NAME} directory")
  set(CPM_forces_SOURCE "${CMAKE_CURRENT_SOURCE_DIR}/../${TARGET_NAME}" CACHE PATH "Local source path for ${TARGET_NAME}.")
else()
  set(CPM_forces_SOURCE "" CACHE PATH "Local source path for ${TARGET_NAME}.")
endif()
CPMAddPackage("https://git.ufz.de/chs/forces.git@0.4.0")
if(BUILD_MPR_LIB_SHARED)
  set_property(TARGET ${TARGET_NAME} PROPERTY POSITION_INDEPENDENT_CODE ON)
endif()
target_link_libraries(${LIB_NAME} PUBLIC ${TARGET_NAME})

# by setting compile options and definitions PUBLIC, they are also used by
# programms linking agains it (mhm exe in this case)
include(../cmake/compileoptions.cmake)
if(CMAKE_Fortran_COMPILER_ID MATCHES "GNU")
  target_compile_definitions(${LIB_NAME} PRIVATE GFORTRAN)
  target_compile_options(${LIB_NAME} PUBLIC
    -ffixed-line-length-none
    $<$<CONFIG:DEBUG>:-Og -g -Wall -Wextra -fimplicit-none -fbacktrace -fcheck=all -ffpe-trap=zero,overflow,underflow -finit-real=snan>
    $<$<CONFIG:RELEASE>:-Ofast>
    $<$<BOOL:${CMAKE_WITH_COVERAGE}>:-g --coverage>
    $<$<BOOL:${CMAKE_WITH_GPROF}>:-pg>
  )
  if(CMAKE_WITH_COVERAGE)
    target_link_libraries(${LIB_NAME} PUBLIC gcov)
  endif()
elseif(CMAKE_Fortran_COMPILER_ID MATCHES "Intel")
  # https://discourse.cmake.org/t/preserving-options-with-spaces-in-add-compile-options/1551/2
  target_compile_definitions(${LIB_NAME} PRIVATE INTEL)
  target_compile_options(${LIB_NAME} PUBLIC
    -nofixed "SHELL:-assume byterecl" "SHELL:-fp-model source" -m64 "SHELL:-assume realloc-lhs"
    $<$<CONFIG:DEBUG>:-g "SHELL:-warn all" "SHELL:-check all" -debug extended -traceback -fp-stack-check -O0 -fstack-protector-all -fstack-security-check -fpe0>
    $<$<CONFIG:RELEASE>:-O3 -qoverride-limits>
  )
elseif(CMAKE_Fortran_COMPILER_ID MATCHES "NAG")
  target_compile_definitions(${LIB_NAME} PRIVATE NAG)
  target_compile_options(${LIB_NAME} PUBLIC
    -colour -unsharedf95 -ideclient -fpp
    $<$<CONFIG:DEBUG>:-g -nan -O0 -C=all -strict95 -ieee=stop>
    $<$<CONFIG:RELEASE>:-O4 -ieee=full>
  )
elseif(CMAKE_Fortran_COMPILER_ID MATCHES "PGI")
  target_compile_definitions(${LIB_NAME} PRIVATE NAG)
  target_compile_options(${LIB_NAME} PUBLIC
    -Mfree -mcmodel=medium
    $<$<CONFIG:DEBUG>:-C -c -g -traceback -O0>
    $<$<CONFIG:RELEASE>:-fast>
  )
endif()

if((CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME OR ${LIB_NAME}_BUILD_TESTING) AND BUILD_TESTING)
  add_subdirectory(./tests)
endif()
