# MPR Release Notes
## MPR v1.0.7 (November 2024)

- fix logging issue with gfortran [MR](https://git.ufz.de/chs/MPR/-/merge_requests/102)

## MPR v1.0.6 (August 2023)

- updated license information [MR](https://git.ufz.de/chs/MPR/-/merge_requests/98)

## MPR v1.0.5 (June 2023)

- removed logging dependency and now integrated updated mo_logging module from FORCES, see [MR](https://git.ufz.de/chs/MPR/-/merge_requests/94)
- HPC module loads now version 1.9
- CMake Fortran Scripts now version 2.0
- fixed bug in calculating the coordinates for case when bound is given, stagger is center and there is no common step size is

## MPR v1.0.4 (September 2022)

- fixed bug in upscaling to irregular grids, see [MR](https://git.ufz.de/chs/MPR/-/merge_requests/93)

## MPR v1.0.3 (September 2022)

- updated documentation
- updated CMake installation similar to mHM using CPM

## MPR v1.0.2 (May 2022)

- updated the cmake_modules repository
- updated the lightweight_fortran_lib to forces

## MPR v1.0.1 (May 2021)

- reintegrated erroneously deleted tests for Python preproprocessors
- fixed some typos in README.md
- updated the hpc-module-loads subrepo
- activated documentation building for master branch by default as well

- ## MPR v1.0 (Mar 2021)

- initial release

