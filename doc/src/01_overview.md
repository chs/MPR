# MPR - Overview

[TOC]

Welcome to the documentation of MPR version \htmlinclude version.txt \latexinclude version.txt.

![MPR logo](doc/figures/mpr_logo.png "MPR logo")

The MPR (Multiscale Parameter Regionalization) tool is a general model pre-processor with the purpose of estimating
distributed parameter fields for (environmental) models.

## Cite as

The model code can be cited as:

* MPR: Schweppe, R., S. Thober, S. Mueller, M. Kelbling, R. Kumar, S. Attinger, and L. Samaniego (2022): MPR 1.0: a stand-alone multiscale parameter regionalization tool for improved parameter estimation of land surface models. Geosci. Model Dev., 15, 859–882, 2022,[https://doi.org/10.5194/gmd-15-859-2022](https://gmd.copernicus.org/articles/15/859/2022/)

Please see the original publication of the MPR framework in Samaniego et al. (2010) and Kumar et al. (2013):

* Samaniego L., R. Kumar, S. Attinger (2010): Multiscale parameter regionalization of a grid-based hydrologic model at the mesoscale. Water Resour. Res., 46,W05523, [doi:10.1029/2008WR007327](http://onlinelibrary.wiley.com/doi/10.1029/2008WR007327/abstract)
* Kumar, R., L. Samaniego, and S. Attinger (2013): Implications of distributed hydrologic model parameterization on water fluxes at multiple scales and locations, Water Resour. Res., 49, [doi:10.1029/2012WR012195](http://onlinelibrary.wiley.com/doi/10.1029/2012WR012195/abstract)

## License

MPR is free software. You can
redistribute it and/or modify it under the terms of the GNU General
Public License as published by the free Software Foundation either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You received a copy of the [GNU Lesser General Public License](../LICENSE) along
with MPR. Thecomplete GNU license text can also be found at  <http://www.gnu.org/licenses/>.
